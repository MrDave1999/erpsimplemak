﻿namespace ERP.Domain.Entities.Commons
{
    public class OperationState
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
