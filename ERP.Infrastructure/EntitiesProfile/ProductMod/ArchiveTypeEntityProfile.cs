﻿using ERP.Domain.Entities.ProductMod;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.Infrastructure.EntitiesProfile.ProductMod
{
    public class ArchiveTypeEntityProfile : IEntityTypeConfiguration<ArchiveType>
    {
        public void Configure(EntityTypeBuilder<ArchiveType> builder)
        {
            builder.Property(x => x.ConcurrencyToken).IsConcurrencyToken();
        }
    }
}
